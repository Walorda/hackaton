var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
var path = require('path'); 
const WebSocket = require('ws');

app.use(express.static('public'))
app.use(function (req, res, next) {
  console.log('middleware');
  req.testing = 'testing';
  return next();
});
 
app.get('/', function(req, res, next){
    res.sendFile('/index.html' , { root : __dirname});
});
app.get('/index2', function(req, res, next){
    res.sendFile('/index2.html' , { root : __dirname});
});
 
let test = [];
app.ws('/', function(ws, req) {
    test.push(ws)
    ws.on('message', function(message) {
    if (message.type === 'utf8') { // accept only text
            // broadcast message to all connected clients
            var json = JSON.stringify({ type:'message', data: "test" });
            ws.send(json)
        }
    
    })
    var json = JSON.stringify({ type:'message', data: "Table 1" });
    var json1 = JSON.stringify({ type:'message', data: "Table 2" });
            ws.send(json)
            ws.send(json1)
    ws.on('error', (e) => {
        console.log(e);
    })

});

const wss = new WebSocket.Server({ port: 8080 })

wss.on('connection', ws => {
  ws.on('message', message => {
    expressWs.getWss().clients.forEach(client => {
        client.send(JSON.stringify({type:message}));
        console.log("test");
    })
    })
  
    
  
});


app.listen(3000);