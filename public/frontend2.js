$(function () {
  "use strict";  // for better performance - to avoid searching in DOM
 
  window.WebSocket = window.WebSocket || window.MozWebSocket;  // if browser doesn't support WebSocket, just show

  var connection = new WebSocket('ws://localhost:8080');
  connection.onopen = function () {
    console.log("Socket Connection open")
    connection.send("table 1");
  }; 

  connection.onerror = function (error) {
    console.log(error);
  };  


  connection.onmessage = function (message) {
    try {
      var json = JSON.parse(message.data);
      console.log(json);
    } catch (e) {
      console.log('Invalid JSON: ', message.data);
      return;
    }
  };

  connection.onclose = function() {
    console.log("connection closed");
  }


  setInterval(function () {
    if (connection.readyState !== 1) {
      console.log("Error connecting");
    }
  }, 3000);



  /**
   * Add message to the chat window
   
  function addMessage(author, message, color, dt) {
    content.prepend('<p><span style="color:' + color + '">'
        + author + '</span> @ ' + (dt.getHours() < 10 ? '0'
        + dt.getHours() : dt.getHours()) + ':'
        + (dt.getMinutes() < 10
          ? '0' + dt.getMinutes() : dt.getMinutes())
        + ': ' + message + '</p>');
  }*/
});